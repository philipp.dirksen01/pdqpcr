data_targets = "" # Add data files here separated by spaces

.PHONY: all
all: $(data_targets)

$(data_targets): data/%.rda: data-raw/%.R
	cd data-raw; Rscript $(<F)

.PHONY: clean
clean:
	rm -rf data/*.rda

.PHONY: test
test:
	Rscript -e "devtools::test()"

.PHONY: doc
doc:
	Rscript -e "devtools::document()"

.PHONY: install
install:
	Rscript -e "if (require(PACKAGENAME)) devtools::uninstall()"
	Rscript -e "devtools::document()"
	Rscript -e "devtools::check()" || true
	Rscript -e "devtools::install(build_vignettes = TRUE, upgrade = 'never')"
